"""Модуль построения графика по данным из файла
    Параметры запуска: 2 текстовых файла с расширением .txt
    Версия интерпретатора: Python 3.7.2 64bit
    URL репозитория для скачивания: https://www.python.org/downloads/release/python-370/
    © 2019. Симонова А. 89080567599 simonova_anastasia97@mail.ru"""

import datetime
from datetime import timedelta
import matplotlib.pyplot as plt
import matplotlib as mpl
import csv
from numpy import *
import sys

mpl.rcParams['agg.path.chunksize'] = 200000 # изменение настроек matplotlib, для предотвращения ошибок рендеринга.
                                                # диапазон значений [0, 100000]

class Chart:
    """Класс График.
Конструктор принимает название файла."""
    def __init__(self, filename):
        self.fileName=filename
        self.arrX = []
        self.arrY = []
        self.file = open(filename, 'r')
        line = self.file.readline()

        if line.find(';')==-1:
            self.nameChart = "IbaAnalizer"
        else:
            self.nameChart = "LMS"
            arr=line.split(';')
            nums=(arr[1]).split('.')
            self.date=nums[0]+'.'+nums[1]+'.'+'20'+nums[2]+' '+arr[2]
            if(arr[2].find('.')==-1):
                self.date+='.000000'

    def parsingIbaAnalizerFile(self):
        """Метод для формирования координат функции из файла IbaAnalizer"""
        try:
            for line in self.file:
                text = (line.replace(',', '.')).split('\t')

                if text[1] == '1.#QNAN\n':
                    if len(self.arrY) == 0:
                        continue
                    y = self.arrY[len(self.arrY) - 1]
                else:
                    y = float(text[1])
                self.arrY.append(y)

                date = datetime.datetime.strptime(text[0], "%d.%m.%Y %H:%M:%S.%f")
                self.arrX.append(date)
        except Exception:
            raise("Некорректные данные в IbaAnalizer файле")

    def parsingLMSFile(self):
        """Метод для формирования координат функции из файла LMS"""
        try:
            n=10
            while n>0:
                str1=self.file.readline()
                n-=1

            mainDate = datetime.datetime.strptime(self.date, "%d.%m.%Y %H:%M:%S.%f")

            for line in self.file:
                data=((line).replace(',','.')).split(';')
                time=(data[1]).split('.')
                msec=float(str(time[1]))
                sec=float(str(time[0]))
                d=mainDate+timedelta(seconds=sec)+timedelta(milliseconds=msec/1000)
                self.arrX.append(d)
                self.arrY.append(float(data[2]))
        except Exception:
            raise("Некорректные данные в LMS файле")

    def drawCharts(self):
        """Метод для отрисовки графика"""
        if(self.nameChart=="IbaAnalizer"):
            self.parsingIbaAnalizerFile()
        else:
            self.parsingLMSFile()
        fig, ch = plt.subplots()
        plt.grid()
        plt.xlabel('Время')
        ch.plot(self.arrX, self.arrY, 'b', label=self.nameChart)
        ch.legend(loc='upper left')
        ch.set_title('Отображение графиков по координатам из файла: \n' + self.fileName)
        ch.set_ylabel(self.nameChart)
        plt.show()

    def drawManyCharts(self, obj2):
        """Метод для отрисовки двух графиков в одном окне."""
        if(self.nameChart=="IbaAnalizer"):
            self.parsingIbaAnalizerFile()
        else:
            self.parsingLMSFile()
        if(obj2.nameChart == "IbaAnalizer"):
            obj2.parsingIbaAnalizerFile()
        else:
            obj2.parsingLMSFile()
        fig, ch = plt.subplots()
        plt.grid()
        plt.xlabel('Время\n\n© 2019. Симонова А. 89080567599 simonova_anastasia97@mail.ru')
        ch2 = ch.twinx()
        ch.plot(self.arrX, self.arrY, 'b', label=self.nameChart)
        ch2.plot(obj2.arrX, obj2.arrY, 'r', label=obj2.nameChart)
        ch2.legend(loc='upper right')
        ch.legend(loc='upper left')
        ch.set_title('Отображение графиков по координатам из файлов: \n'+ self.fileName+', '+obj2.fileName+'\n')

        ch.set_ylabel(self.nameChart)
        ch2.set_ylabel(obj2.nameChart)
        plt.show()

    def createCSV(self, obj2):
        with open('persons.csv', 'w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',', lineterminator='\r')

            i=0
            j=0
            n=0
            while j<len(self.arrY): #obj2 - LMS (i)
                #filewriter.writerow(['Name', 'Profession'])
                if i==0:
                    filewriter.writerow([str(self.arrY[j]), str(obj2.arrY[i])])
                    i+=6
                    j+=1
                else:
                    filewriter.writerow([str(self.arrY[j]), str(obj2.arrY[i])])
                    j+=1
                    i+=5

    def correl(self, obj2):
        i = 0
        j = 0
        commonArr=[]
        while j < len(self.arrY) and i< len(obj2.arrY) and j<2847802:  # obj2 - LMS (i)
            if i==0:
                commonArr.append([self.arrY[j], obj2.arrY[i]])
                i += 6
                j += 1
            else:
                commonArr.append([self.arrY[j], obj2.arrY[i]])
                j += 1
                i += 5
        k = corrcoef([a[0] for a in commonArr], [a[1] for a in commonArr])[0, 1]
        print(commonArr)
        print(k)

    def correl2(self, obj2):
        i = 0
        j = 0
        commonArr = []
        while j < len(self.arrY) and i < len(obj2.arrY) and j < 9327802:  # obj2 - LMS (i)
            if j<7887802:
                if i == 0:
                    i += 6
                    j += 1
                else:
                    j += 1
                    i += 5
            else:
                commonArr.append([self.arrY[j], obj2.arrY[i]])
                j += 1
                i += 5

        k = corrcoef([a[0] for a in commonArr], [a[1] for a in commonArr])[0, 1]
        print(commonArr)
        print(k)

    def pickPoints(self):
        interval=datetime.datetime.strptime('15.02.2019 20:07:41.000000', "%d.%m.%Y %H:%M:%S.%f")
        pick=-100000
        pickY=0
        arr1=[] #x
        arr2=[] #y
        i=0
        fig, ch = plt.subplots()
        plt.grid()
        plt.xlabel('Время')
        for value in self.arrX:

            if value<=interval:
                if pick<self.arrY[i]:
                    pick=self.arrY[i]
                    pickY=value
                    i+=1
            else:
                arr1.append(pick)
                arr2.append(pickY)
                pick=-10000
                i+=1
                interval += timedelta(minutes=5)

        plt.scatter(arr2, arr1)
        plt.show()

    def pickPoints2(self, obj2):
        interval=datetime.datetime.strptime('15.02.2019 20:03:41.000000', "%d.%m.%Y %H:%M:%S.%f")
        pick=-100000
        pickY=0
        arr1=[] #x
        arr2=[] #y
        i=0
        fig, ch = plt.subplots()
        plt.grid()
        plt.xlabel('Время')

        for value in self.arrX:

            if value<=interval:
                if pick<self.arrY[i]:
                    pick=self.arrY[i]
                    pickY=value
                i+=1
            else:
                arr1.append(pick)
                arr2.append(pickY)
                pick=-10000
                i+=1
                interval += timedelta(minutes=3)

        ch.scatter(arr2, arr1)

        ch2 = ch.twinx()


        pick2=-100000
        pickY2=0
        arr4=[] #x
        arr3=[] #y
        i=0
        interval = datetime.datetime.strptime('15.02.2019 20:03:41.000000', "%d.%m.%Y %H:%M:%S.%f")
        for value in obj2.arrX:

            if value<=interval:
                if pick2<obj2.arrY[i]:
                    pick2=obj2.arrY[i]
                    pickY2=value
                i+=1
            else:
                arr4.append(pick2)
                arr3.append(pickY2)
                pick2=-10000
                i+=1
                interval += timedelta(minutes=3)

        ch.scatter(arr2, arr1)
        ch2.scatter(arr3, arr4)
        plt.show()

    def pirsonOnSections(self, obj2, interval):
        time=0
        i=0 # self
        j=0 # obj2
        if self.arrX[i]>obj2.arrX[j]:
            time=self.arrX[0] + timedelta(minutes=interval)
            while self.arrX[i]>obj2.arrX[j]:
                j+=1
        else:
            time=obj2.arrX[0] + timedelta(minutes=interval)
            while self.arrX[i]<obj2.arrX[j]:
                i+=1

        pirsonsArr = []
        commonArr=[]

        while i<len(self.arrX) and j<len(obj2.arrX):
            while self.arrX[i]<time and i<len(self.arrX) and j<len(obj2.arrX):
                if i == 0:
                    commonArr.append([self.arrY[i], obj2.arrY[j]])
                    j += 6
                    i += 1
                else:
                    commonArr.append([self.arrY[i], obj2.arrY[j]])
                    i += 1
                    j += 5
            k = corrcoef([a[0] for a in commonArr], [a[1] for a in commonArr])[0, 1]
            print(commonArr)
            commonArr.clear()
            time+=timedelta(minutes=interval)
            pirsonsArr.append(round(k,3))

        print(pirsonsArr)
        print('моды: ')
        z=0
        z1=1
        mods=[]
        z2=0
        while z<len(pirsonsArr)-1:
            while z1<len(pirsonsArr):
                if pirsonsArr[z]==pirsonsArr[z1]:
                    if len(mods)==0:
                        mods.append(pirsonsArr[z])
                    else:
                        check=False
                        while z2<len(mods):
                            if mods[z2]==pirsonsArr[z]:
                                check=True
                            z2+=1
                        if check==False:
                            mods.append(pirsonsArr[z])
                        z2=0
                z1+=1
            z+=1

        print(mods)

        numPlus=0
        numMinus=0
        num0=0
        for value in pirsonsArr:
            if value>=0.5:
                numPlus+=1
            elif value<=-0.5:
                numMinus+=1
            else:
                num0+=1

        print("длина массива: "+str(len(pirsonsArr)))
        print("1: "+str(numPlus/(len(pirsonsArr) /100))+'%')
        print("0: " + str(num0/(len(pirsonsArr) / 100))+'%')
        print("-1: " + str(numMinus/(len(pirsonsArr) / 100))+'%')

    def findDeltaMinMin(self, obj2, interval):
        mainTime=0
        i=0 # self
        j=0 # obj2
        if self.arrX[i]>obj2.arrX[j]:
            mainTime=self.arrX[0] + timedelta(minutes=interval)
            while self.arrX[i]>obj2.arrX[j]:
                j+=1
        else:
            mainTime=obj2.arrX[0] + timedelta(minutes=interval)
            while self.arrX[i]<obj2.arrX[j]:
                i+=1

        pick=100000
        pickX=0
        arr1=[] #y
        arr2=[] #x
        time=mainTime

        while i < len(self.arrX):

            if self.arrX[i]<=time:
                if pick>self.arrY[i]:
                    pick=self.arrY[i]
                    pickX=self.arrX[i]
                i+=1
            else:
                arr1.append(pick)
                arr2.append(pickX)
                pick=100000
                i+=1
                time += timedelta(minutes=interval)
        print(i)

        pick2=100000
        pickX2=0
        arr4=[] #x2
        arr3=[] #y2
        time=mainTime

        while j < len(obj2.arrX):

            if obj2.arrX[j]<=time:
                if pick2>obj2.arrY[j]:
                    pick2=obj2.arrY[j]
                    pickX2=obj2.arrX[j]
                j+=1
            else:
                arr4.append(pick2)
                arr3.append(pickX2)
                pick2=100000
                j+=1
                time += timedelta(minutes=interval)

        #arr2, arr3
        deltatime=[]
        i=0
        while i<len(arr2) and i<len(arr3):
            deltatime.append(arr2[i]-arr3[i])
            i+=1

        print(deltatime)

    def findDeltaMaxMax(self, obj2, interval):
        mainTime=0
        i=0 # self
        j=0 # obj2
        if self.arrX[i]>obj2.arrX[j]:
            mainTime=self.arrX[0] + timedelta(minutes=interval)
            while self.arrX[i]>obj2.arrX[j]:
                j+=1
        else:
            mainTime=obj2.arrX[0] + timedelta(minutes=interval)
            while self.arrX[i]<obj2.arrX[j]:
                i+=1

        pick=-100000
        pickX=0
        arr1=[] #y
        arr2=[] #x
        time=mainTime

        while i < len(self.arrX):

            if self.arrX[i]<=time:
                if pick<self.arrY[i]:
                    pick=self.arrY[i]
                    pickX=self.arrX[i]
                i+=1
            else:
                arr1.append(pick)
                arr2.append(pickX)
                pick=-100000
                i+=1
                time += timedelta(minutes=interval)
        print(i)

        pick2=-100000
        pickX2=0
        arr4=[] #x2
        arr3=[] #y2
        time=mainTime

        while j < len(obj2.arrX):

            if obj2.arrX[j]<=time:
                if pick2<obj2.arrY[j]:
                    pick2=obj2.arrY[j]
                    pickX2=obj2.arrX[j]
                j+=1
            else:
                arr4.append(pick2)
                arr3.append(pickX2)
                pick2=-100000
                j+=1
                time += timedelta(minutes=interval)

        #arr2, arr3
        deltatime=[]
        i=0
        while i<len(arr2) and i<len(arr3):
            deltatime.append(arr2[i]-arr3[i])
            i+=1

        print(deltatime)

obj=Chart("ibaAnalyzer_абсолютноевремя2.txt")
obj2=Chart("LMSTestExpress.txt")
#obj=Chart("3.txt")
#obj2=Chart("2.txt")
obj2.drawManyCharts(obj)
obj2.findDeltaMinMin(obj, 3)
#obj2.createCSV(obj)
#obj2.correl2(obj)
obj.pickPoints2(obj2)
#obj.pirsonOnSections(obj2, 3)

#if __name__ == "__main__":
#    if len(sys.argv) == 3:
#        file1 = sys.argv[1]
#        file2 = sys.argv[2]
#        obj=Chart(file1)
#        obj2=Chart(file2)
#        obj.drawManyCharts(obj2)
#    elif len(sys.argv) == 2:
#        file1 = sys.argv[1]
#        obj = Chart(file1)
#        obj.drawCharts()
#    else:
#        print("Некорректные данные.")
#        sys.exit(1)
